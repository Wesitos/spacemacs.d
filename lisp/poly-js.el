;;; poly-markdown.el --- Polymode for markdown-mode -*- lexical-binding: t -*-
;;
;; Author: Vitalie Spinu
;; Maintainer: Vitalie Spinu
;; Copyright (C) 2018
;; Version: 0.1.5
;; Package-Requires: ((emacs "25") (polymode "0.1.5") (markdown-mode "2.3"))
;; URL: https://github.com/polymode/poly-markdown
;; Keywords: emacs
;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file is *NOT* part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(require 'polymode)
(require 'js2-mode)
(require 'rjsx-mode)


(defun poly-js-resolve-mode-from-tag (tag)
  (pcase tag
    ("gql" 'graphql)
    ((pred (string-match "styled\\.\\w+")) 'css)
    ))

(defun poly-js-mode-matcher ()
  ;; Get the mode from the head-match
  (message "Mode-matcher")
  (if (match-beginning 2)
      (poly-js-resolve-mode-from-tag (match-string-no-properties 2))))

(defun poly-js-template-string-code-head-matcher (count)
  (let ((template-start-regex
         "[[:space:]^,;:!|&><=*/%+-]\\(\\([[:alpha:]_][[:alpha:][:digit:]_.]+\\)[[:space:]]*`[[:space:]]*\n\\)"))
    (when
        (and (re-search-forward template-start-regex nil t count)
             (save-match-data
               (let ((bol (point-at-bol)))
                 (not (re-search-backward js2-mode-//-comment-re bol t)))))
      (cons (match-beginning 1) (match-end 1))))
  )

(defun poly-js-template-string-code-tail-matcher (count)
  (if (and
       (match-beginning 1)
       (re-search-forward
        "[^\\]\\(`\\)\\|\\(\\(\\${\\)\\)" nil t count))
      ;; False if matched an ${
      (cons (match-beginning 1) (match-end 1))))


(defcustom pm-inner/js-template-string-code
  (pm-inner-auto-chunkmode
   :name "js-template-string-code"
   :head-matcher #'poly-js-template-string-code-head-matcher
   :tail-matcher #'poly-js-template-string-code-tail-matcher
   :mode-matcher #'poly-js-mode-matcher
   :head-mode 'host
   :tail-mode 'host)
  "Js template string code block."
  :group 'poly-innermodes
  :type 'object)

(defcustom pm-host/jsx
  (pm-host-chunkmode :name "jsx"
                     :mode 'rjsx-mode)
  "JSX hostmode."
  :group 'poly-hostmodes
  :type 'object)

(defcustom pm-poly/js
  (pm-polymode :name "javascript"
               :hostmode 'pm-host/js
               :innermodes '(pm-inner/js-template-string-code))
  "Javascript typical configuration"
  :group 'polymodes
  :type 'object)

(defcustom pm-poly/jsx
  (pm-polymode :name "jsx"
               :hostmode 'pm-host/jsx
               :innermodes '(pm-inner/js-template-string-code))
  "JSX typical configuration"
  :group 'polymodes
  :type 'object)



;;;###autoload  (autoload 'poly-js-mode "poly-js")
(define-polymode poly-js-mode pm-poly/js)
;;;###autoload  (autoload 'poly-jsx-mode "poly-js")
(define-polymode poly-jsx-mode pm-poly/jsx)

(provide 'poly-js)
